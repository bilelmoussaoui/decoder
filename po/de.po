# German translation for decoder.
# Copyright (C) 2021 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the decoder package.
# Translators
# Milo Ivir <mail@milotype.de>, 2021.
# Philipp Kiemle <philipp.kiemle@gmail.com>, 2021-2022.
# Jürgen Benvenuti <gastornis@posteo.org>, 2022-2024.
msgid ""
msgstr ""
"Project-Id-Version: decoder\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/Decoder/-/issues\n"
"POT-Creation-Date: 2024-10-17 07:00+0000\n"
"PO-Revision-Date: 2024-10-17 10:10+0200\n"
"Last-Translator: Jürgen Benvenuti <gastornis@posteo.org>\n"
"Language-Team: German <gnome-de@gnome.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.4.4\n"

#: data/com.belmoussaoui.Decoder.desktop.in.in:3
#: data/com.belmoussaoui.Decoder.metainfo.xml.in.in:7
#: data/resources/ui/history_page.ui:14 data/resources/ui/window.ui:18
#: data/resources/ui/window.ui:34 src/application.rs:49 src/main.rs:18
msgid "Decoder"
msgstr "Decoder"

#: data/com.belmoussaoui.Decoder.desktop.in.in:4 src/application.rs:41
msgid "Scan and Generate QR Codes"
msgstr "QR-Codes scannen und erstellen"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/com.belmoussaoui.Decoder.desktop.in.in:10
msgid "QR;Scan;Generate;Code;"
msgstr "QR;scannen;erstellen;Code;"

#: data/com.belmoussaoui.Decoder.gschema.xml.in:6
#: data/com.belmoussaoui.Decoder.gschema.xml.in:7
msgid "Default window width"
msgstr "Standard-Fensterbreite"

#: data/com.belmoussaoui.Decoder.gschema.xml.in:11
#: data/com.belmoussaoui.Decoder.gschema.xml.in:12
msgid "Default window height"
msgstr "Standard-Fensterhöhe"

#: data/com.belmoussaoui.Decoder.gschema.xml.in:16
msgid "Default window maximized behavior"
msgstr "Standardverhalten des maximierten Fensters"

#: data/com.belmoussaoui.Decoder.metainfo.xml.in.in:8
msgid "Scan and generate QR codes"
msgstr "QR-Codes scannen und erstellen"

#: data/com.belmoussaoui.Decoder.metainfo.xml.in.in:10
msgid "Fancy yet simple QR Codes scanner and generator."
msgstr ""
"Schickes und doch einfaches Programm zum Scannen und Erstellen von QR-Codes."

#: data/com.belmoussaoui.Decoder.metainfo.xml.in.in:11
msgid "Features:"
msgstr "Funktionen:"

#: data/com.belmoussaoui.Decoder.metainfo.xml.in.in:13
msgid "QR Code generation"
msgstr "QR-Code erstellen"

# Müsste das dann im Original nicht Scanning with a camera heißen? - jb
# https://gitlab.gnome.org/World/decoder/-/merge_requests/54 - pk
#: data/com.belmoussaoui.Decoder.metainfo.xml.in.in:14
msgid "Scanning with a camera"
msgstr "Mit einer Kamera scannen"

#: data/com.belmoussaoui.Decoder.metainfo.xml.in.in:15
msgid "Scanning from a screenshot"
msgstr "Von einem Bildschirmfoto scannen"

#: data/com.belmoussaoui.Decoder.metainfo.xml.in.in:16
msgid "Parses and displays QR code content when possible"
msgstr "QR-Code analysieren und wenn möglich den Inhalt anzeigen"

#: data/com.belmoussaoui.Decoder.metainfo.xml.in.in:22
msgid "Generating a QR Code"
msgstr "QR-Code erstellen"

#: data/com.belmoussaoui.Decoder.metainfo.xml.in.in:26
msgid "Scanning QR Code with a camera"
msgstr "QR-Code mit Kamera scannen"

#: data/com.belmoussaoui.Decoder.metainfo.xml.in.in:30
msgid "History of the scanner/generated QR codes"
msgstr "Verlauf der gescannten/erstellten QR-Codes"

#: data/resources/ui/camera_page.ui:22
msgid "Missing Camera Permission"
msgstr "Fehlende Kamera-Berechtigung"

#: data/resources/ui/camera_page.ui:23
msgid "Allow camera usage in Settings"
msgstr ""
"Bitte erteilen Sie die Berechtigung zum Verwenden der Kamera in den "
"Einstellungen"

#: data/resources/ui/camera_page.ui:26 data/resources/ui/camera_page.ui:50
msgid "_From a Screenshot…"
msgstr "Aus einem _Bildschirmfoto …"

#: data/resources/ui/camera_page.ui:47
msgid "No Camera Found"
msgstr "Keine Kamera gefunden"

#: data/resources/ui/camera_page.ui:86
msgid "Select Camera"
msgstr "Kamera auswählen"

#: data/resources/ui/camera_page.ui:98
msgid "Capture From a Screenshot"
msgstr "Aus einem Bildschirmfoto erfassen"

#: data/resources/ui/help-overlay.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "Allgemein"

#: data/resources/ui/help-overlay.ui:14
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Tastenkombinationen anzeigen"

#: data/resources/ui/help-overlay.ui:20
msgctxt "shortcut window"
msgid "Quit"
msgstr "Beenden"

#: data/resources/ui/help-overlay.ui:28
msgctxt "shortcut window"
msgid "Codes"
msgstr "Codes"

#: data/resources/ui/help-overlay.ui:31
msgctxt "shortcut window"
msgid "Scan QR Code"
msgstr "QR-Code scannen"

#: data/resources/ui/history_page.ui:15
msgid "Decode a QR code or create one first"
msgstr "Einen QR-Code decodieren oder zuerst einen erstellen"

#: data/resources/ui/qrcode_create.ui:46
msgid "_Text"
msgstr "_Text"

#: data/resources/ui/qrcode_create.ui:56
msgid "_Wi-Fi"
msgstr "_WLAN"

#: data/resources/ui/qrcode_create.ui:73
#: data/resources/ui/qrcode_scanned_page.ui:96
msgid "_Export"
msgstr "_Exportieren"

#: data/resources/ui/qrcode_create.ui:83
msgid "_Save"
msgstr "_Sichern"

#: data/resources/ui/qrcode_create.ui:84
msgid "Save in History"
msgstr "Im Verlauf speichern"

#: data/resources/ui/qrcode_kind_event.ui:23
msgid "Summary"
msgstr "Zusammenfassung"

#: data/resources/ui/qrcode_kind_event.ui:52
msgid "Starts At"
msgstr "Beginnt um"

#: data/resources/ui/qrcode_kind_event.ui:79
msgid "Ends At"
msgstr "Endet um"

#: data/resources/ui/qrcode_kind_event.ui:106
msgid "Location"
msgstr "Standort"

#: data/resources/ui/qrcode_kind_event.ui:133
msgid "Description"
msgstr "Beschreibung"

#: data/resources/ui/qrcode_kind_location.ui:23
msgid "Latitude"
msgstr "Breitengrad"

#: data/resources/ui/qrcode_kind_location.ui:49
msgid "Longitude"
msgstr "Längengrad"

#: data/resources/ui/qrcode_kind_mail.ui:23
msgid "Email"
msgstr "E-Mail"

#: data/resources/ui/qrcode_kind_mail.ui:52
msgid "Subject"
msgstr "Betreff"

#: data/resources/ui/qrcode_kind_mail.ui:81
msgid "Body"
msgstr "Text"

#: data/resources/ui/qrcode_kind_sms.ui:23
#: data/resources/ui/qrcode_kind_telephone.ui:23
msgid "Phone"
msgstr "Telefon"

#: data/resources/ui/qrcode_kind_sms.ui:50
msgid "Content"
msgstr "Inhalt"

#: data/resources/ui/qrcode_kind_text.ui:24 data/resources/ui/text_page.ui:18
msgid "Text"
msgstr "Text"

#: data/resources/ui/qrcode_kind_url.ui:23
msgid "Website"
msgstr "Webseite"

#: data/resources/ui/qrcode_kind_wifi.ui:23
msgid "Network"
msgstr "Netzwerk"

#: data/resources/ui/qrcode_kind_wifi.ui:51
msgid "Encryption"
msgstr "Verschlüsselung"

#: data/resources/ui/qrcode_row.ui:44
#: data/resources/ui/qrcode_scanned_page.ui:65
msgid "Copy Contents"
msgstr "Inhalt kopieren"

#: data/resources/ui/qrcode_row.ui:53
msgid "_Export…"
msgstr "_Exportieren …"

#: data/resources/ui/qrcode_row.ui:60
msgid "_Delete"
msgstr "_Löschen"

#: data/resources/ui/qrcode_scanned_page.ui:4 src/widgets/qrcode/widget.rs:70
msgid "QR Code"
msgstr "QR-Code"

#: data/resources/ui/qrcode_scanned_page.ui:50
msgid "Contents"
msgstr "Inhalt"

#: data/resources/ui/wifi_page.ui:10
msgid "Network Name"
msgstr "Netzwerkname"

#: data/resources/ui/wifi_page.ui:16
msgid "Password"
msgstr "Passwort"

#: data/resources/ui/wifi_page.ui:22
msgid "_Hidden"
msgstr "_Versteckt"

#: data/resources/ui/wifi_page.ui:23
msgid "Is this a hidden Wi-Fi network?"
msgstr "Ist das ein verborgenes WLAN-Netzwerk?"

#: data/resources/ui/wifi_page.ui:31
msgid "_Encryption Algorithm"
msgstr "V_erschlüsselungsalgorithmus"

#: data/resources/ui/window.ui:5
msgid "_Keyboard Shortcuts"
msgstr "_Tastenkombinationen"

#: data/resources/ui/window.ui:9
msgid "_About Decoder"
msgstr "_Info zu Decoder"

#: data/resources/ui/window.ui:49
msgid "Main Menu"
msgstr "Hauptmenü"

#: data/resources/ui/window.ui:62
msgid "_Create"
msgstr "_Erstellen"

#: data/resources/ui/window.ui:76
msgid "_Scan"
msgstr "_Scannen"

#: data/resources/ui/window.ui:90
msgid "_History"
msgstr "_Verlauf"

#: src/application.rs:47
msgid "translator-credits"
msgstr ""
"Milo Ivir <mail@milotype.de>\n"
"Philipp Kiemle <philipp.kiemle@gmail.com>\n"
"Tim Sabsch <tim@sabsch.com>\n"
"Jürgen Benvenuti <gastornis@posteo.org>"

#. NOTE This is in the context of selecting a wifi encryption
#. algorithm.
#: src/qrcode_kind.rs:335
msgid "None"
msgstr "Keiner"

#: src/widgets/qrcode/create.rs:120 src/widgets/qrcode/scanned_page.rs:124
msgid "Could not generate QR code"
msgstr "QR-Code konnte nicht erstellt werden"

#: src/widgets/qrcode/create.rs:164 src/widgets/qrcode/scanned_page.rs:119
msgid "QR Code saved in history"
msgstr "QR-Code im Verlauf gespeichert"

#: src/widgets/qrcode/row.rs:135
msgid "Copied to clipboard"
msgstr "In Zwischenablage kopiert"

#: src/widgets/window.rs:262
msgid "_Select"
msgstr "A_uswählen"

#. TRANSLATORS This a file name, do not translate the file format (.png)
#: src/widgets/window.rs:264
msgid "qr-code.png"
msgstr "qr-code.png"

#~ msgid "Bilal Elmoussaoui"
#~ msgstr "Bilal Elmoussaoui"

#~ msgid "Unknown Device"
#~ msgstr "Unbekanntes Gerät"

#~ msgid "Create New Bibliography"
#~ msgstr "Neue Bibliografie erstellen"

#~ msgid "Cancel"
#~ msgstr "Abbrechen"

#~ msgid "E-Mail"
#~ msgstr "E-Mail"

#~ msgid "Enable or disable dark mode"
#~ msgstr "Dunklen Modus aktivieren oder deaktivieren"

#~ msgid "_Visible"
#~ msgstr "_Sichtbar"

#~ msgid "Toggle Dark Mode"
#~ msgstr "Dunklen Modus umschalten"

#~ msgctxt "shortcut window"
#~ msgid "Dark Mode"
#~ msgstr "Dunkler Modus"

#~ msgid "_Copy"
#~ msgstr "_Kopieren"

#~ msgid "QR Code scanning through a camera"
#~ msgstr "QR-Code mit Kamera scannen"
