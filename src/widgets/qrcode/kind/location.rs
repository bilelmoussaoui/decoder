// SPDX-License-Identifier: GPL-3.0-or-later
use gtk::{glib, subclass::prelude::*};

use crate::qrcode_kind::Location;

mod imp {
    use super::*;

    #[derive(Default, gtk::CompositeTemplate)]
    #[template(resource = "/com/belmoussaoui/Decoder/ui/qrcode_kind_location.ui")]
    pub struct QRCodeLocation {
        #[template_child]
        pub longitude_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub latitude_label: TemplateChild<gtk::Label>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for QRCodeLocation {
        const NAME: &'static str = "QRCodeLocation";
        type Type = super::QRCodeLocation;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }
    impl ObjectImpl for QRCodeLocation {}
    impl WidgetImpl for QRCodeLocation {}
    impl BoxImpl for QRCodeLocation {}
}

glib::wrapper! {
    pub struct QRCodeLocation(ObjectSubclass<imp::QRCodeLocation>) @extends gtk::Widget, gtk::Box;
}

impl QRCodeLocation {
    pub fn new(location: Location) -> Self {
        let widget = glib::Object::new::<QRCodeLocation>();
        widget.init(location);
        widget
    }

    fn init(&self, location: Location) {
        let self_ = self.imp();
        self_
            .longitude_label
            .set_label(&location.longitude.to_string());
        self_
            .latitude_label
            .set_label(&location.latitude.to_string());
    }
}
