// SPDX-License-Identifier: GPL-3.0-or-later
use gtk::{glib, subclass::prelude::*};

use crate::qrcode_kind::WiFi;

mod imp {
    use super::*;

    #[derive(Default, gtk::CompositeTemplate)]
    #[template(resource = "/com/belmoussaoui/Decoder/ui/qrcode_kind_wifi.ui")]
    pub struct QRCodeWiFi {
        #[template_child]
        pub network_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub encryption_label: TemplateChild<gtk::Label>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for QRCodeWiFi {
        const NAME: &'static str = "QRCodeWiFi";
        type Type = super::QRCodeWiFi;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }
    impl ObjectImpl for QRCodeWiFi {}
    impl WidgetImpl for QRCodeWiFi {}
    impl BoxImpl for QRCodeWiFi {}
}

glib::wrapper! {
    pub struct QRCodeWiFi(ObjectSubclass<imp::QRCodeWiFi>) @extends gtk::Widget, gtk::Box;
}

impl QRCodeWiFi {
    pub fn new(wifi: WiFi) -> Self {
        let widget = glib::Object::new::<QRCodeWiFi>();
        widget.init(wifi);
        widget
    }

    fn init(&self, wifi: WiFi) {
        let self_ = self.imp();

        self_.network_label.set_label(&wifi.network);
        self_
            .encryption_label
            .set_label(&wifi.encryption.to_string());
    }
}
