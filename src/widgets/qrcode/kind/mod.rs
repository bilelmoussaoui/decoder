// SPDX-License-Identifier: GPL-3.0-or-later
mod event;
mod location;
mod mail;
mod sms;
mod telephone;
mod text;
mod url;
mod wifi;

pub use event::QRCodeEvent;
pub use location::QRCodeLocation;
pub use mail::QRCodeMail;
pub use sms::QRCodeSms;
pub use telephone::QRCodeTelephone;
pub use text::QRCodeText;
pub use wifi::QRCodeWiFi;

pub use self::url::QRCodeUrl;
